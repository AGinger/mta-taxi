if ($('#map').length && $(window).width() > 767) {
mapboxgl.accessToken = 'pk.eyJ1Ijoid2hpdGVsYWJlbGRldmVsb3BlcnMiLCJhIjoiY2s0d3pqNzVqMGJrdjNsbXpyMDZxcGVoaiJ9.mPCJPnmlPvXbI39Si-cnBQ';
var map = new mapboxgl.Map({
    container: 'map',
    //style: 'mapbox://styles/mapbox/streets-v11',
    center: [37.530424,
        55.757882], 
    zoom: 15,
    style: 'mapbox://styles/whitelabeldevelopers/ckbp6xniy3nee1jnzn7vv7mvm'
});

var geojson = {
    "type": "FeatureCollection",
    "features": [
    {
    "type": "Feature",
    "properties": {
    "iconImage": 'url(./images/map/mark-1.png)'
    },
    "geometry": {
    "type": "Point",
    "coordinates": [
        37.530424,
        55.758882
    ]
    }
    },
    {
    "type": "Feature",
    "properties": {
    "iconImage": 'url(./images/map/mark-2.png)'
    },
    "geometry": {
    "type": "Point",
    "coordinates": [
        37.525818,
        55.756449
        
    ]
    }
    }
    ]
    };
    
    // add markers to map
    var $num = 0;
    geojson.features.forEach(function(marker) {

        if ($num === 0) {
            // create a DOM element for the marker
            var el = document.createElement('div');
            el.className = 'marker';
            el.style.backgroundImage = marker.properties.iconImage;
            el.style.backgroundSize = '139px 41px';
            el.style.width = '139px';
            el.style.height = '41px';
            
            // el.addEventListener('click', function() {
            // window.alert(marker.properties.message);
            // });
            
            // add marker to map
            new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .addTo(map);
        }
        else {
            // create a DOM element for the marker
            var el = document.createElement('div');
            el.className = 'marker';
            el.style.backgroundImage = marker.properties.iconImage;
            el.style.backgroundSize = '68px 51px';
            el.style.width = '68px';
            el.style.height = '51px';
            
            // el.addEventListener('click', function() {
            // window.alert(marker.properties.message);
            // });
            
            // add marker to map
            new mapboxgl.Marker(el)
            .setLngLat(marker.geometry.coordinates)
            .addTo(map);
        }

        $num++;
    
    });

}