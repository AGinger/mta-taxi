$('.close-menu, .show-hide').click(function (e) { 
    e.preventDefault();
    $('body').toggleClass('open-menu');
    $('.navi').fadeToggle();
});

$('.order-car a, .pop-form-close').click(function (e) { 
    e.preventDefault();
    $('body').toggleClass('open-menu');
    
    $('.pop-body').fadeToggle();
});

$('.send-order').click(function (e) { 
    e.preventDefault();
    
    $('#order').fadeOut(0);
    $('#thank').fadeIn();
});

$('.pop-form-close').click(function (e) { 
    e.preventDefault();
    
    $('#thank').fadeOut(0);
    $('#order').fadeIn();
});

$('.phone-inp').mask('0 000 0000000');
$('.number-inp').mask('00 SS 000000');
$('.cat-inp').mask('S0');